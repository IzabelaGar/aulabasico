﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Animal
    {
        //Key
        public int AnimalID { get; set; }

        public String QualAnimal { get; set; }
        public String Raça { get; set; }
        public String Nome { get; set; }
        public String Cor { get; set; }
        public String Porte { get; set; }
        public String DescriçãoAdicional { get; set; }
        public String NomeDono { get; set; }
        public float TelefoneDono { get; set; }
        public char EmailDono { get; set; }

    }
}