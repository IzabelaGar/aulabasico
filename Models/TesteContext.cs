﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class TesteContext : DbContext
    {
        public TesteContext() : base("name=ConnIza")
        {

        }

        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet <Animal>Animais { get; set; }

    }
}