﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Pessoa
    {
        //[Key]
        public int PessoaID { get; set; }

        public string Nome { get; set; }

        public DateTime DataNacs { get; set; }

        public float Telefone { get; set; }

        public char Endereco { get; set; }

        public char Email { get; set; }
    }
}