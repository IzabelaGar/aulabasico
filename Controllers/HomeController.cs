﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Class1ViewModel teste = new Class1ViewModel();
            teste.HoraTeste = DateTime.Now.AddYears(-38);
            teste.Idade = 12 * 8;
            teste.Nome = "ze";


            return View(teste);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {


            return Content("teste");
        }
    }
}